## Python library requirements

* Flask
* gunicorn = concurrent web server. Needs a wsgi.py
* flask-oauthlib https://github.com/lepture/flask-oauthlib/blob/master/example/github.py

## Listening to requests

* WSGI (Flask listener not for prod) => wsgi.py; openshift will detect that file and run it via gunicorn (in requirements.txt)
* If using other web server, make it listen on 0.0.0.0:8080

## oauth2

* registration
* API use?

## Create app in Openshift

* warning about public project, KB about using private repositories in Openshift

```bash
oc new-app python:2.7~https://gitlab.cern.ch/alossent/openshift-flask-oauth-ex.git
oc create route edge --insecure-policy=Redirect --service=openshift-flask-oauth-ex
oc set env dc/openshift-flask-oauth-ex CERN_OAUTH_CLIENT_ID=XXXXX CERN_OAUTH_CLIENT_SECRET=XXXXXX
```