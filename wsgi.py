'''WSGI exectuable

A container built from the Openshift python image will look for the Gunicorn web server
at startup, and start it to listen for incoming requests on 0.0.0.0:8080.

Gunicorn (http://gunicorn.org/) will in turn pass requests to the Python application via WSGI (Web Server
Gateway Interface)

By default gunicorn will use wsgi.py as entry point. This can be overriden by setting
environment variable APP_MODULE. See http://docs.gunicorn.org/en/stable/run.html#gunicorn
for acceptable values.
'''

# Simply import the Flask instance as 'application'.
# The Flask instance already implements the expected interface.
from flaskapp import app as application